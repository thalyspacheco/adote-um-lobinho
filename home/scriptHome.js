var direction = true

function generateLobinho(lobo) {

    let lobinhos = document.querySelector(".lobinhos")

    let wolfExemple = document.createElement('div')

    let wolfPic = document.createElement('figure')
    
    let wolfData = document.createElement('div')

    

    if (direction) {
        wolfExemple.classList.add("wolfExemple")
        wolfPic.classList.add("wolfPic")
        wolfData.classList.add("wolfData")
        direction = false
    } else {
        wolfExemple.classList.add("exempleWolf")
        wolfPic.classList.add("picWolf")
        wolfData.classList.add("dataWolf")
        direction = true
    }
    

    

    let image = document.createElement('img')
    image.setAttribute('src', lobo.image_url)
    image.setAttribute('width', '430')
    image.setAttribute('height', '352')
    wolfPic.append(image)

    wolfExemple.append(wolfPic)

   

    let wolfName = document.createElement('p')
    wolfName.classList.add("wolfName")
    wolfName.innerText = lobo.name
    wolfData.append(wolfName)
    let wolfAge = document.createElement('p')
    wolfAge.classList.add("wolfAge")
    wolfAge.innerText = "Idade: " + lobo.age + " anos"
    wolfData.append(wolfAge)
    let wolfDescription = document.createElement('p')
    wolfDescription.classList.add("wolfDescription")
    wolfDescription.innerText = lobo.description

    wolfData.append(wolfDescription)

    wolfExemple.append(wolfData)

    lobinhos.append(wolfExemple)
}

const urlApi = "https://lobinhos.herokuapp.com/wolves/"

async function getWolfById(id) {

    let fetchConfig = {
        method: "GET"
    }

    fetch(urlApi + id, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => { generateLobinho(resp) })
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })
}

getWolfById(78)
getWolfById(76)