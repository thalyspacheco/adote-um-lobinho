const urlApi = "https://lobinhos.herokuapp.com/wolves/"

var wolfId = sessionStorage.getItem("wolfId")

async function getWolfById(id) {

    let fetchConfig = {
        method: "GET"
    }

    fetch(urlApi + id, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => { generateLobinho(resp) })
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })
}

function generateLobinho(lobo) {

    let lobinhos = document.querySelector(".lobinhos")

    let wolfPic = document.createElement('figure')
    wolfPic.classList.add("wolfPic")

    let image = document.createElement('img')
    image.setAttribute('src', lobo.image_url)
    image.setAttribute('width', '200')
    image.setAttribute('height', '200')
    wolfPic.append(image)

    let wolfName = document.createElement('p')
    wolfName.classList.add("wolfName")
    wolfName.innerText = "Adote o(a) " + lobo.name

    lobinhos.append(wolfPic)
    lobinhos.append(wolfName)
}

getWolfById(wolfId)

 function adoptWolfById(id) {

    let adptName = document.querySelector(".adopterName").value
    let adptAge = document.querySelector(".adopterAge").value
    let adptEmail = document.querySelector(".adopterEmail").value

    let fetchBody = {
        adopter_name: adptName,
        adopter_age: adptAge,
        adopter_email: adptEmail,
        adopted: true
    }

    let fetchConfig = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fetchBody)
    }

    fetch(urlApi + id, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => { console.log(resp) })
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })

}

let adoptBtn = document.querySelector(".adoptBtn")
adoptBtn.addEventListener("click", () => {
    adoptWolfById(wolfId)
})

