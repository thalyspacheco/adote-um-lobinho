const urlApi = "https://lobinhos.herokuapp.com/wolves/"

let wolfId = 297
sessionStorage.setItem("wolfId", wolfId)

async function getWolfById(id) {

    let fetchConfig = {
        method: "GET"
    }

    fetch(urlApi + id, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => { generateLobinho(resp) })
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })
}

async function deleteWolfById(id) {

    let fetchConfig = {
        method: "DELETE"
    }

    fetch(urlApi + id, fetchConfig)
        .then(resposta => resposta.json()
            .then(resp => {console.log(resp)})
            .catch(error => { console.log(error) })
        )
        .catch(error => { console.log(error) })
}

function generateLobinho(lobo) {

    let lobinhos = document.querySelector(".lobinhos")

    let wolfExemple = document.createElement('div')
    wolfExemple.classList.add("wolfExemple")

    let wolfPic = document.createElement('figure')
    wolfPic.classList.add("wolfPic")

    let image = document.createElement('img')
    image.setAttribute('src', lobo.image_url)
    image.setAttribute('width', '301')
    image.setAttribute('height', '247')
    wolfPic.append(image)

    wolfExemple.append(wolfPic)

    let options = document.createElement('div')
    options.classList.add("options")
    let button1 = document.createElement('button')
    let button2 = document.createElement('button')
    button1.classList.add("btnAdopt")
    button2.classList.add("btnExclude")
    button1.innerText = "Adotar"
    button2.innerText = "Excluir"

    options.append(button1)
    options.append(button2)

    let wolfData = document.createElement('div')
    wolfData.classList.add("wolfData")

    let wolfName = document.createElement('p')
    wolfName.classList.add("wolfName")
    wolfName.innerText = lobo.name

    let wolfDescription = document.createElement('p')
    wolfDescription.classList.add("wolfDescription")
    wolfDescription.innerText = lobo.description

    wolfData.append(wolfDescription)

    wolfExemple.append(wolfData)

    lobinhos.append(wolfName)
    lobinhos.append(wolfExemple)
    lobinhos.append(options)

    button1.addEventListener("click", () => {
        location.assign("../adotarLobinho/adotarLobinho.html")
    })

    button2.addEventListener("click", () => {
        deleteWolfById(lobo.id)
    })
}

getWolfById(wolfId)